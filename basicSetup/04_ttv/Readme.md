# Running the Train Trust Validator

clone the TTV git repo: `git clone git@gitlab.eclipse.org:eclipse/xfsc/train/trusted-content-resolver.git`

Open the repo in an editor of your choice and modify the file src/main/resources/application.yaml

## Start Universal Resolver images

First, switch to the uni-resolver folder:

```sh
cd uni-resolver
```

If you want to run **all** univeral resolver images:

```sh
docker compose --env-file unires.env -f uni-resolver-all.yml up -d
```

If you only need the did web resolver:

```sh
docker compose --env-file unires.env -f uni-resolver-web.yml up -d
```

## Verify it works
To test did:web resolution you can try the following CURL commands:

``` sh
curl -X GET http://localhost:8080/1.0/identifiers/did:web:did.actor:alice
curl -X GET http://localhost:8080/1.0/identifiers/did:web:did.actor:bob
curl -X GET http://localhost:8080/1.0/identifiers/did:web:did.actor:mike
```

## Starting the Train Trust validator image

Go back into the base folder and run:

```sh
docker compose up -d
```
