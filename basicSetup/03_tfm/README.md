# Setup and Run TFM for Federation 1:

First we will need to modify the configuration to work for our federation1. 
To do this open the file `federation1/docker-compose.yml` and do the following:

Set the Zonemanager API Key info:
```yaml
    environment:
       SPRING_SECURITY_OAUTH2_RESOURCESERVER_JWT_ISSUER_URI: http://keycloak:8080/realms/gxfs-dev-test
       TSPA_IPFS_RCP_API: /ip4/172.30.0.4/tcp/5001
       NAMESERVER_TOKEN: HERE YOU MUST SET YOUR API KEY!
       NAMESERVER_ADDRESS: http://fed1-dns:16001
       REQUEST_GET_MAPPING: http://fed1-tfm:16003/tspa-service/tspa/v1/
       STORAGE_TYPE_TRUSTLIST: IPFS
```

Change into the `federation1` folder and run: `docker compose up -d`

# Setup and Run TFM for Federation 2:

First we will need to modify the configuration to work for our federation2. 
To do this open the file `federation2/docker-compose.yml` and do the following:

Set the Zonemanager API Key info:
```yaml
    environment:
       SPRING_SECURITY_OAUTH2_RESOURCESERVER_JWT_ISSUER_URI: http://keycloak:8080/realms/gxfs-dev-test
       TSPA_IPFS_RCP_API: /ip4/172.30.0.4/tcp/5001
       NAMESERVER_TOKEN: HERE YOU MUST SET YOUR API KEY!
       NAMESERVER_ADDRESS: http://fed2-dns:16001
       REQUEST_GET_MAPPING: http://fed2-tfm:16003/tspa-service/tspa/v1/
       STORAGE_TYPE_TRUSTLIST: IPFS
```

Change into the `federation2` folder and run: `docker compose up -d`

