# Running Federation1
1. Opening a Terminal in the 02_TDZM/federation1 folder
2. Start the TDZM with `docker compose up -d fed1-dns`
3. Get the api key generated via: `docker logs fed1-dns` **REMEMBER THIS KEY FOR THE TFM SETUP**
4. Enter the API Key into the 02_TDZM/federation1 ui.env config file
5. Start the ui with: `docker compose up -d`

Before you can make requests via Postman you must open the "Federation1" 
environment of the postman collection and set the dns_api_key value to the API Key generated.

You can now check out it is running by logs, the ui or the postman health status.


# Running Federation2
1. Opening a Terminal in the 02_TDZM/federation2 folder
2. Start the TDZM with `docker compose up -d fed2-dns`
3. Get the api key generated via: `docker logs fed2-dns` **REMEMBER THIS KEY FOR THE TFM SETUP**
4. Enter the API Key into the 02_TDZM/federation2 ui.env config file
5. Start the ui with: `docker compose up -d`

Before you can make requests via Postman you must open the "Federation2" environment of the postman collection and set the dns_api_key value to the API Key generated.

You can now check out it is running by logs, the ui or the postman health status.

